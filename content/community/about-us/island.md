+++
title = "The Island of TeX"
description = "The Island of TeX is is a warm and welcoming place for TeX-related projects."
draft = false
weight = 10
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "The Island of TeX is a warm and welcoming place for TeX-related projects."
toc = true
top = false
+++

# A picturesque scenery within the TeX ecosystem



# More than our projects

At the Island of TeX, we focus on the community aspect. We might not be a large
community yet, but we are trying to be helpful and responsive. Bug reports,
feature requests, questions about our tools, and general discussions about TeX
and its ecosystem are always welcome. For us, even our tools are not about the
cool tech but about how we help users.

As such, you as our user are valuable to us in shaping the future of our vision
of the TeX ecosystem. In turn, our vision of the TeX ecosystem is what drives us
to build new tools or improve existing ones. The Island of TeX is as much about
ideas and exchange of thoughts as it is about the current state of the projects.

So…

# Always looking to grow

If you like TeX, see missing pieces in the TeX ecosystem, or want to start
contributing to open-source software in a welcoming environment, we would love
to hear from you. The island is always open for tourists, regular visitors, and
new residents.

How we could benefit from you, and what you could get:

* Thinking of creating a new TeX-related project, especially in TeX
  tooling/ecosystem? Consider joining us and get started in an environment with
  a nice community, connections to all the knowledgeable people in the world of
  TeX, and mutual benefit in terms of publicity.
* Want to join open-source development? We have a number of projects with
  (partially) diverse technology stacks. All of which have open issues. Because
  resolving them helps us, we also happily provide mentoring for common tools
  (git, pre-commit, GitLab, …), the programming languages, and our code bases.
* Responsibility is important to you? We are always looking for active
  maintainers for some of our projects. By taking over maintainership you get to
  control the action and set the pace for development in a part of the island.
* TeX and its ecosystem has its problem and you want to discuss them? Sure, we
  are always happy to hear from you. Those discussions may lead to new project
  ideas for us, or even wider discussions at a TUG conference or in TUGboat, for
  which we will happily support your efforts.

Anything of this sounds interesting? Or maybe you have another reason to stop
by? Find us in our [Matrix community room][matrix]. We would love to see you
there.

[matrix]: https://matrix.to/#/!titTeSvZiqNOvRIKCv:matrix.org?via=matrix.org
