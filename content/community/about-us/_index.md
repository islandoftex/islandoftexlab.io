+++
title = "About us"
description = "About the Island of TeX, its members and its goals."
template = "docs/section.html"
sort_by = "weight"
weight = 1
draft = false
+++
