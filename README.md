# The Island of TeX's website

## Repository contents

This respository contains by and large the following components:

* blog posts and description pages served at https://islandoftex.gitlab.io,
* static assets belonging to the website,
* custom [Zola](https://www.getzola.org) shortcodes and templates,
* customizations to the [AdiDoks](https://github.com/aaranxu/adidoks) theme.

Build the site yourself using `zola build`.

## License

* Our templates and modifications to the AdiDoks theme (directory `templates`)
  are subject to the [MIT license][mit].
* Static content (directory `static`) is all rights reserved unless otherwise
  stated (e.g. in blog posts referencing the static content). The files from the
  [KaTeX project](https://katex.org) (`katex*` and `auto-render.min.js` in
  `static`) are subject to the [MIT license][mit].
* All our projects' descriptions (in directory `content/community/projects`) are
  released under the terms of the respective project's license.
* The pages about the vision of the Island of TeX and its members (files
  `island.md` and `members.md` in directory `content/community/about-us`) are
  licensed under the terms of [CC BY-NC-ND 4.0][cc-by-nc-nd].
* All remaining content of this website is licensed under the terms of
  [CC BY 4.0][cc-by].

[cc-by]: https://creativecommons.org/licenses/by/4.0/
[cc-by-nc-nd]: https://creativecommons.org/licenses/by-nc-nd/4.0/
[mit]: https://opensource.org/licenses/MIT
