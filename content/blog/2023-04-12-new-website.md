+++
title = "The island's new website"
description = "A new website, new visual identity, and new horizons."
template = "blog/page.html"

[extra]
lead = """\
After more than three years and being known to quite a number of “known faces” \
in the TeX community, we finally present our official website to present the \
Island of TeX to the wider public. With this website comes a new visual \
identity which will successively be rolled out to all our projects.\
"""
+++

# A website and what it means to us

The Island of TeX is all about the TeX community and providing tools to its
ecosystem. We have always tried and will always try our best to be present in
the TeX community's most important media: the TUG (TeX user group) conferences
and the TUGboat journal. But let us face a simple fact: most TeX users these
days are no TeX users but LaTeX users, and most of them are not attending TUG or
reading TUGboat.

Still, many of them are accessing tools provided by us, be it
[texdoc.org](https://texdoc.org/), the TeX Live docker images which are part of
many CI workflows, or even our beloved bird arara. The question we always ask
ourselves is simple: do people actually know this is part of the Island of TeX
community effort? How can we place the Island of TeX in a more prominent
position for further development of modern TeX infrastructure?

So in the times of the internet we came to answer our questions with a simple
fact: people are using the internet to gather information about their tools,
communities, etc. So we need to be present on the internet. And, although it may
sound a little 2000-ish to create a plain, simple website as our primary
presence on the web without social media or anything, here we are.

Essentially, this website is meant to present our community to the wider public.
And in the course of it enable us to write some blog posts about our progress,
collect our publications over different media, and attract new members who want
to join the island. Or inofficially, who want to become islanders.

If you are reading this, maybe you are even one of the potential candidates. So
in case you are interested, join us in our [matrix chat
room](https://matrix.to/#/!titTeSvZiqNOvRIKCv:matrix.org?via=matrix.org). We are
always happy to welcome new users or chat with those who use our tools.

# On the road towards a new visual identity

Speaking of tools, the Island of TeX is best known for its oldest and greatest
project: arara. In 2012, arara started out with this playful and colorful bird
complemented by the word arara in
[Comfortaa](https://fonts.google.com/specimen/Comfortaa) font. The manual used
some compact serif and the dominant color was green – the color of hope.

Indeed, ten years later this hope seems to have been worth it. arara matured and
is now part of the Island of TeX. We are determined to be a warm and welcoming
place for TeX projects, yet also a place for the future of the TeX tooling
ecosystem. While arara's initial design worked for the first part – being warm
and cozy – we are convinced that we can do better to convey being a modern and
vibrant community.

The most noticable change in our thinking is that we aspire to aim for a
consistent visual identity, something we missed to target for quite a while. All
our projects mix and match visuals as we saw fit when creating the respective
project. As we want all of our projects to be perceived as part of the Island of
TeX, we want to proceed with a unified visual identity.

And while some of our ideas (concerning fonts, colors etc.) have not made the
cut for this website—okay, actually none made it—, we are planning to tackle the
following things related to a new visual identity in the near(ish) future:

- a custom beamer theme to use for our presentations which means at least once a
  year at TUG,
- customizing this website's design to look less like an average documentation
  website,
- adapting arara's website as well based on the overall design decisions for the
  island,
- revamping our projects README's and documentations, bringing even more of them
  to the web as well,
- and what we think of when doing all of the above, ideas always welcome.

As these tasks are less technical, and more TeXnical, than many of our usual
(programming) issues, let us stress that support/helping hands in any of the
directions are more than welcome.

# Thank you for reading and our service for the community

If, after all this text, you have landed down here, we are convinced that you
are truly interested in a vibrant TeX community. Therefore, we want to make the
following offer to TeX content creators, be it blogs, videos, or something else:
If you have some interesting TeX-related content of yours to share, drop us a
line and we will happily put it on our blog here too.
