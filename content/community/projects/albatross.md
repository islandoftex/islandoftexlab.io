+++
title = "albatross"
description = "Find fonts that contain a given (Unicode) glyph."
draft = false
weight = 30
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = '<a href="https://gitlab.com/islandoftex/albatross">Albatross</a> is a tool for finding fonts that contain a given (Unicode) glyph.'
toc = true
top = false
readme_url = "https://gitlab.com/islandoftex/albatross/-/blob/master/README.md"
+++
