% arara: pdflatex
% arara: pdflatex
\documentclass[final]{ltugboat}
\def\REST{\texorpdfstring{\acro{REST}}{REST}}
\def\JAR{\texorpdfstring{\acro{JAR}}{JAR}}
\def\JSON{\texorpdfstring{\acro{JSON}}{JSON}}
\def\API{\texorpdfstring{\acro{API}}{API}}
\usepackage{microtype}
\usepackage{graphicx}
\usepackage[breaklinks,hidelinks,pdfa]{hyperref}
\usepackage{cleveref}

\DeclareUnicodeCharacter{9001}{$\langle$}
\DeclareUnicodeCharacter{9002}{$\rangle$}

\title{\TeX doc online\Dash a web interface for serving \TeX\ documentation}

\author{Island of \TeX\ (developers)}
\EDITORnoaddress
\netaddress{https://gitlab.com/islandoftex}
\personalURL{}

\newcommand\texdocnet{\href{https://texdoc.net}{\texttt{texdoc.net}}}
\newcommand\texdoc{\texttt{texdoc}}

\begin{document}

\maketitle

\begin{abstract}
When looking for \TeX-related documentation, users have many options, including
running \texdoc\ on their local machine, looking up the package
at \CTAN, or using a service like \texdocnet. As the latter is known for lacking
regular updates, the Island of \TeX\ decided to take the opportunity to provide a
complete rewrite of the provided service using a \acro{REST}ful \API\ and a self-updating
Docker container.
\end{abstract}

\section{Core features of \texdocnet}

The most important feature of \texdocnet\ is the documentation search as the
prominent first item on the landing page (cf.\ \cref{fig:texdocnet}).
Searching for a package yields a table with the entries a user could retrieve
locally by executing \verb!texdoc -l! \meta{package}. Each entry is linked to the
corresponding document and the user is able to either view (if there is browser
view support for the file type) or download the documentation file. This is
especially useful for users without a local \TeX\ installation like Overleaf
users.

\begin{figure}[htb]
  \centering
  \includegraphics[width=.95\linewidth]{img/texdoc.png}
  \caption{Screenshot of (top portion of) \texdocnet}
  \label{fig:texdocnet}
\end{figure}

A little bit more direct is the use of the \HTTP\ endpoint
\verb!https://texdoc.net/pkg/!\meta{package}; this responds with the file that
\verb!texdoc <package>! would open if executed locally. The user does not get to
choose what to open but in most cases \texdoc\ is good at determining the
proper documentation if given the package name, so users get what they want.

The aforementioned feature of creating simple references to the documentation is
what makes the service well suited for writing posts on web forums or sites like
the \TeX\ StackExchange. Linking to \CTAN\ mirrors gives far longer urls with many
unnecessary path components compared to the short syntax of \texdocnet.

The last noteworthy feature allows users to browse packages by topics. The list
is retrieved from the (unmaintained) \verb|texdoctk.dat| file available in the underlying
\TeX~Live distribution. Within each topic a selection of packages is shown with
the name, description and link to the main documentation for each. The topics
and presented packages are not exhaustive and many packages on \CTAN\ or even in
\TeX~Live will not be presented to the user.

\section{Providing a \REST{}ful \API\ for \texdoc}
\label{sec:api}

A \REST{}ful \API\ is a stateless interface to web applications responding to typical
\HTTP\ requests with uniform responses. Usually, \JSON\ is used for the response
format. Following this principle, our software responds to \acro{HTTP GET} requests
with \JSON\ representing documentation-related objects.

The endpoints you can access are described as follows. Keep in mind that these requests
will return either \HTTP\ status code 200 (\acro{OK}) or, in the case of any error, \HTTP\
status code 422 (Unprocessable Entity). The only endpoint that is guaranteed not
to fail is located at \verb!/version!.

\begin{description}
  \item[\texttt{/version}] This endpoint returns the versions of the \API\ and
  data format (\verb!api!), the installed version of \texdoc\ (\verb!texdoc!) and the
  date of the last \TeX~Live update as an \ISO\ date string (\verb!tlpdb!). Make sure
  your client software always expects the correct \API\ version to avoid
  problems. Our \API\ versions follow semantic versioning with all its
  consequences.
  
  \item[\texttt{/texdoc/}\meta{name}] On this path, the list of entries is returned
  that a local call to \verb!texdoc -l! would produce. For each entry, there
  are two fields: \texttt{path}, containing the path to the documentation file relative to
  the \verb!doc! subfolder of the \verb!TEXMFDIST! directory; and \texttt{description}
  containing the file's description if there is one (empty otherwise). The
  application will always return a \JSON\ array of such entries.
  
  \item[\texttt{/serve/\meta{name}/\meta{index}}] This call results in 
  the documentation file at index \meta{index} of the
  result of \texttt{/texdoc/\meta{name}} being returned to the client.
  \item[\texttt{/pkg/\meta{name}}] This endpoint is a shortcut for the
  \texttt{/serve/\meta{name}/0} endpoint, defined to preserve
  compatibility with the \API\ of \texdocnet.
  
  \item[\texttt{/topics/list}] This endpoint returns the list of topics known to
  the application specified by their key and a caption called details. This is a
  direct interface to \CTAN's \API\ for topics. Network access for the server
  software is required.
  
  \item[\texttt{/topic/\meta{name}}] This endpoint returns details for a topic by
  returning the key (what is passed as \texttt{\meta{name}}), a string with a
  short description called \texttt{details} and a list of package names (strings) called
  \texttt{packages}. This is a direct interface to \CTAN's \API\ for topics. Network access
  for the server software is required.
\end{description}

\section{The new front end}

The front end of \TeX doc online is structured in a similar way to \texdocnet.
The main feature is still searching for packages. This is based on the
\verb!/texdoc/!\meta{package} endpoint presented in the previous section. The results
will be the same as on \texdocnet.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.95\linewidth]{img/texdoc-online.png}
  \caption{Screenshot of (top portion of) texdoc-online}
  \label{fig:texdoconline}
\end{figure}

Topics are handled differently, though.
%For partitioning the available \TeX\ packages 
We use \CTAN's \acro{JSON API} to fetch their topics and packages belonging to
these topics. Any user visiting the landing page will be shown six random
categories with a few packages each. If a category holds several packages,
four of them are selected at random.  Users have the option to show all topics
and list the packages for any topic they are interested in. Also, each package entry
can be automatically queried for documentation. This covers
many more packages than the old \texttt{texdoctk.dat} file, though it
does have the disadvantage that some of \CTAN's topics are
sparsely populated.

The front page also offers a ``jump to documentation'' section introducing the
\API\ and options to host the software yourself. These are covered in the
next section.

\section{Deploying the software}

The source code for \TeX doc online is hosted at GitLab and may be found at
\url{https://gitlab.com/islandoftex/images/texdoc-online}. There are also
download options to get an executable \JAR\ file with all dependencies included
that you can simply run using your local Java installation. Running the \JAR\
bundle will open a local web server that can be accessed using the url
\url{http://127.0.0.1:8080} from your browser. It will not work without Internet
access (because of how it fetches topics) or a local \TeX\ installation (for
\TeX doc) though.

As an alternative to running the \JAR\ file we also provide a dockerized image
that users can pull using (line break is editorial; this is a one-line string)
\begin{verbatim}
registry.gitlab.com/islandoftex/images/
texdoc-online:latest
\end{verbatim}
This image is based on our \TeX~Live images, which makes it quite large in terms
of file size but also eliminates the need for a local \TeX\ installation. Using
Docker is the preferred solution for hosting your own instance of \TeX doc
online.

The Docker image also provides daily updates. The container
will update daily at midnight (container time) and thus stays up to date as long as the current
version of \TeX\ Live is supported. Our continuous integration ensures that you
can always pull the \verb!latest! tag and receive the latest and greatest \TeX\
documentation, which when pulled and run will update itself.

To ease deployment we provide a ready-made \texttt{docker-compose.yml}
configuration for Docker Compose (\url{https://docs.docker.com/compose/}), an
orchestration tool for Docker containers. It uses the Caddy web server to
provide automatic \acro{HTTPS} with local certificates or, on public domains, Let's
Encrypt certificates. Alter the \texttt{localhost} line within that file
to suit your needs and \texttt{docker-compose up} will start a
production-ready instance of \TeX doc online.

\section{Final remarks}

In cooperation with Stefan Kottwitz we are working on providing a hosted
solution as a future replacement of \texdocnet. Stay tuned for seeing this
transition happen within the next months.

\TeX doc online is still a work in progress and there is room for improvement.
We are working on new features as well as considering ways to extend the current
front-end for additional, hosting-based content. The \REST{}ful \API, through the endpoints
presented in~\cref{sec:api}, allows external applications and services to easily query
\TeX\ package documentation from an updated lookup system.

Feedback can be provided through the project repository in GitLab. We look
forward to hearing from you!

\medskip
\makesignature
\end{document}
